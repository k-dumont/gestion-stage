<?php

namespace AppBundle\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use AppBundle\Traits\UserTrait;

/**
 * @ORM\Entity
 * @ORM\Table(name="pedagogical_referent")
 */
class PedagogicalReferent extends BaseUser implements EntityInterface
{

    use UserTrait;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="phone", type="string", length=255, nullable=true)
     */
    private $phone;

    private $superAdmin;

    /**
     * PedagogicalReferent constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param $phone
     * @return $this
     */
    public function setPhone($phone)
    {

        $this->phone = $phone;

        return $this;
    }

    /**
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }


    /**
     * Set superAdmin
     *
     * @param string $superAdmin
     *
     * @return PedagogicalReferent
     */
    public function setSuperAdmin($superAdmin)
    {
        $this->superAdmin = $superAdmin;

        return $this;
    }

    /**
     * Get superAdmin
     *
     * @return string
     */
    public function getSuperAdmin()
    {
        return $this->superAdmin;
    }
}
