<?php

namespace AppBundle\Entity;

/**
 * Interface EntityInterface
 * @package AppBundle\Entity
 */
interface EntityInterface
{
    /**
     * @return integer
     */
    public function getId();
}
