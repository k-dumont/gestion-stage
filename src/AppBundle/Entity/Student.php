<?php

namespace AppBundle\Entity;

use AppBundle\Traits\ContactTrait;
use AppBundle\Traits\UserTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use AppBundle\Traits\AddressTrait;

/**
 * Student
 *
 * @ORM\Table(name="student")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\StudentRepository")
 */
class Student implements EntityInterface
{
    use AddressTrait;
    use UserTrait;
    use ContactTrait;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="bac_getting", type="datetime")
     */
    private $bacGetting;

    /**
     * @ORM\OneToMany(targetEntity="Stage", mappedBy="student")
     */
    private $stage;

    /**
     * @ORM\ManyToOne(targetEntity="Bacalaureat", cascade={"persist"})
     */
    private $bacalaureat;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->stage = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set bacGetting
     *
     * @param \DateTime $bacGetting
     *
     * @return Student
     */
    public function setBacGetting($bacGetting)
    {
        $this->bacGetting = $bacGetting;

        return $this;
    }

    /**
     * Get bacGetting
     *
     * @return \DateTime
     */
    public function getBacGetting()
    {
        return $this->bacGetting;
    }

    /**
     * @return mixed
     */
    public function getBacalaureat()
    {
        return $this->bacalaureat;
    }

    /**
     * @param Bacalaureat $bacalaureat
     */
    public function setBacalaureat(Bacalaureat $bacalaureat)
    {
        $this->bacalaureat = $bacalaureat;
    }

    /**
     * Add stage
     *
     * @param \AppBundle\Entity\Stage $stage
     *
     * @return Student
     */
    public function addStage(\AppBundle\Entity\Stage $stage)
    {
        $this->stage[] = $stage;

        return $this;
    }

    /**
     * Remove stage
     *
     * @param \AppBundle\Entity\Stage $stage
     */
    public function removeStage(\AppBundle\Entity\Stage $stage)
    {
        $this->stage->removeElement($stage);
    }

    /**
     * Get stage
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getStage()
    {
        return $this->stage;
    }
}
