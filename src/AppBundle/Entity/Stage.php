<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Stage
 *
 * @ORM\Table(name="stage")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\StageRepository")
 */
class Stage implements EntityInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * One Stage has One Company.
     * @ORM\ManyToOne(targetEntity="Company")
     */
    private $company;

    /**
     * One Stage has One PedagogicalReferent.
     * @ORM\ManyToOne(targetEntity="PedagogicalReferent")
     */
    private $pedagogicalReferent;

    /**
     * One Stage has One ProfessionalReferent.
     * @ORM\ManyToOne(targetEntity="ProfessionalReferent")
     */
    private $professionalReferent;

    /**
     * @ORM\ManyToMany(targetEntity="Technologie", cascade={"persist"})
     */
    private $technologies;

    /**
     * @ORM\ManyToOne(targetEntity="Student", cascade={"persist"}, inversedBy="stage")
     */
    private $student;

    /**
     * @ORM\OneToMany(targetEntity="Visite", mappedBy="stage")
     * @ORM\JoinColumn(name="id", referencedColumnName="id")
     */
    private $visite;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="start_date", type="datetime")
     */
    private $startDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="end_date", type="datetime")
     */
    private $endDate;

    /**
     * One Stage has One PedagogicalReferent.
     * @ORM\ManyToOne(targetEntity="Promo")
     */
    private $promo;

    /**
     * Stage constructor.
     */
    public function __construct()
    {
        $this->visite = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return Company
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * @param Company $company
     */
    public function setCompany(Company $company)
    {
        $this->company = $company;
    }

    /**
     * @return PedagogicalReferent
     */
    public function getPedagogicalReferent()
    {
        return $this->pedagogicalReferent;
    }

    /**
     * @param PedagogicalReferent $pedagogicalReferent
     */
    public function setPedagogicalReferent(PedagogicalReferent $pedagogicalReferent)
    {
        $this->pedagogicalReferent = $pedagogicalReferent;
    }

    /**
     * @return ProfessionalReferent
     */
    public function getProfessionalReferent()
    {
        return $this->professionalReferent;
    }

    /**
     * @param ProfessionalReferent $professionalReferent
     */
    public function setProfessionalReferent(ProfessionalReferent $professionalReferent)
    {
        $this->professionalReferent = $professionalReferent;
    }

    /**
     * Set startDate
     *
     * @param \DateTime $startDate
     *
     * @return Stage
     */
    public function setStartDate($startDate)
    {
        $this->startDate = $startDate;

        return $this;
    }

    /**
     * Get startDate
     *
     * @return \DateTime
     */
    public function getStartDate()
    {
        return $this->startDate;
    }

    /**
     * Set endDate
     *
     * @param \DateTime $endDate
     *
     * @return Stage
     */
    public function setEndDate($endDate)
    {
        $this->endDate = $endDate;

        return $this;
    }

    /**
     * Get endDate
     *
     * @return \DateTime
     */
    public function getEndDate()
    {
        return $this->endDate;
    }

    /**
     * @return mixed
     */
    public function getPromo()
    {
        return $this->promo;
    }

    /**
     * @param Promo $promo
     */
    public function setPromo(Promo $promo)
    {
        $this->promo = $promo;
    }

    /**
     * @return ArrayCollection
     */
    public function getVisite()
    {
        return $this->visite;
    }


    /**
     * Add visite
     *
     * @param \AppBundle\Entity\Visite $visite
     *
     * @return Stage
     */
    public function addVisite(\AppBundle\Entity\Visite $visite)
    {
        $this->visite[] = $visite;

        return $this;
    }

    /**
     * Remove visite
     *
     * @param \AppBundle\Entity\Visite $visite
     */
    public function removeVisite(\AppBundle\Entity\Visite $visite)
    {
        $this->visite->removeElement($visite);
    }

    /**
     * Set student
     *
     * @param \AppBundle\Entity\Student $student
     *
     * @return Stage
     */
    public function setStudent(\AppBundle\Entity\Student $student = null)
    {
        $this->student = $student;

        return $this;
    }

    /**
     * Get student
     *
     * @return \AppBundle\Entity\Student
     */
    public function getStudent()
    {
        return $this->student;
    }

    /**
     * Add technology
     *
     * @param \AppBundle\Entity\Technologie $technology
     *
     * @return Stage
     */
    public function addTechnology(\AppBundle\Entity\Technologie $technology)
    {
        $this->technologies[] = $technology;

        return $this;
    }

    /**
     * Remove technology
     *
     * @param \AppBundle\Entity\Technologie $technology
     */
    public function removeTechnology(\AppBundle\Entity\Technologie $technology)
    {
        $this->technologies->removeElement($technology);
    }

    /**
     * Get technologies
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTechnologies()
    {
        return $this->technologies;
    }
}
