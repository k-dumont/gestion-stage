<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use AppBundle\Traits\UserTrait;

/**
 * @ORM\Entity
 * @ORM\Table(name="professional_referent")
 */
class ProfessionalReferent implements EntityInterface
{

    use UserTrait;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @var string
     *
     * @ORM\Column(name="professional_referent_fonction", type="string", length=255)
     */
    protected $professionalReferentFonction;

    /**
     * @return string
     */
    public function getProfessionalReferentFonction()
    {
        return $this->professionalReferentFonction;
    }

    /**
     * @param string $professionalReferentFonction
     */
    public function setProfessionalReferentFonction($professionalReferentFonction)
    {
        $this->professionalReferentFonction = $professionalReferentFonction;
    }

}
