<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ConnexionLog
 *
 * @ORM\Table(name="connexion_log")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ConnexionLogRepository")
 */
class ConnexionLog implements EntityInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="client_ip", type="string", length=255)
     */
    private $clientIp;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="connexion_datetime", type="datetime")
     */
    private $connexionDatetime;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set clientIp
     *
     * @param string $clientIp
     *
     * @return ConnexionLog
     */
    public function setClientIp($clientIp)
    {
        $this->clientIp = $clientIp;

        return $this;
    }

    /**
     * Get clientIp
     *
     * @return string
     */
    public function getClientIp()
    {
        return $this->clientIp;
    }

    /**
     * Set connexionDatetime
     *
     * @param \DateTime $connexionDatetime
     *
     * @return ConnexionLog
     */
    public function setConnexionDatetime($connexionDatetime)
    {
        $this->connexionDatetime = $connexionDatetime;

        return $this;
    }

    /**
     * Get connexionDatetime
     *
     * @return \DateTime
     */
    public function getConnexionDatetime()
    {
        return $this->connexionDatetime;
    }
}

