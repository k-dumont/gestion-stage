<?php

namespace AppBundle\Manager;

use AppBundle\Entity\EntityInterface;
use Doctrine\Common\Persistence\ObjectRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMInvalidArgumentException;
use Psr\Log\LoggerInterface;

/**
 * Class GlobalEntityManager
 * @package AppBundle\Manager
 */
class GlobalEntityManager
{
    /**
     * @var EntityManagerInterface
     */
    protected $em;

    /**
     * @var string $entity
     */
    protected $entityClass;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * GlobalEntityManager constructor.
     * @param EntityManagerInterface $entityManager
     * @param LoggerInterface $logger
     */
    public function __construct(EntityManagerInterface $entityManager, LoggerInterface $logger)
    {
        $this->em = $entityManager;
        $this->logger = $logger;
    }

    /**
     * @return ObjectRepository|null
     */
    public function getRepository()
    {
        if ($this->entityClass) {
            return $this->em->getRepository($this->entityClass);
        }

        return null;
    }

    /**
     * @param $entityClass
     * @return $this
     */
    public function setEntityClass($entityClass)
    {
        $this->entityClass = $entityClass;

        return $this;
    }

    /**
     * Enregistrement / Update d'une entité (Contenant une clé primaire nommée ID)
     *
     * @param EntityInterface $entity
     * @return bool
     */
    public function save(EntityInterface $entity)
    {
        return $this->processResponse(function () use ($entity) {
            if (!$entity->getId()) {
                $this->em->persist($entity);
            }

            $this->em->flush();
        });
    }

    /**
     * @param EntityInterface $entity
     * @return bool
     */
    public function remove(EntityInterface $entity)
    {
        return $this->processResponse(function () use ($entity) {
            $this->em->remove($entity);
            $this->em->flush();
        });
    }

    /**
     * @param callable $action
     * @return bool
     */
    protected function processResponse(callable $action)
    {
            call_user_func($action);
        try {
            return true;

        } catch (OptimisticLockException $e) {

            $this->logger->error($e->getMessage());
            return false;

        } catch (ORMInvalidArgumentException $e) {

            $this->logger->error($e->getMessage());
            return false;
        }
    }

    /**
     * @param array $criteria
     * @return bool
     */
    public function actionBehavior(array $criteria)
    {
        return ($this->getRepository()->findOneBy($criteria)) ? true : false;
    }


}
