<?php

namespace AppBundle\Manager;

use AppBundle\Entity\PedagogicalReferent;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class PedagogicalReferentManager extends GlobalEntityManager
{
    /**
     * @var PedagogicalReferent
     */
    protected $entityClass = PedagogicalReferent::class;

    /**
     * @var UserPasswordEncoderInterface
     */
    private $passwordEncoder;

    /**
     * @param EntityManagerInterface $entityManager
     * @param LoggerInterface $logger
     * @param UserPasswordEncoderInterface $passwordEncoder
     */
    public function __construct(EntityManagerInterface $entityManager, LoggerInterface $logger, UserPasswordEncoderInterface $passwordEncoder)
    {
        parent::__construct($entityManager, $logger);
        $this->passwordEncoder = $passwordEncoder;
    }

    /**
     * @param PedagogicalReferent $pedagogicalReferent
     * @return bool
     */
    public function savePedagogicalReferent(PedagogicalReferent $pedagogicalReferent)
    {
        if ($pedagogicalReferent->getId()) {

            if ($pedagogicalReferent->getPlainPassword()) {
                $encodedPassword = $this->passwordEncoder->encodePassword(
                    $pedagogicalReferent,
                    $pedagogicalReferent->getPlainPassword()
                );
                $pedagogicalReferent->setPassword($encodedPassword);
            }
        } else {
            $encodedPassword = $this->passwordEncoder->encodePassword(
                $pedagogicalReferent,
                $pedagogicalReferent->getPlainPassword()
            );
            $pedagogicalReferent->setPassword($encodedPassword);
        }

        $pedagogicalReferent->setUsername($pedagogicalReferent->getEmail());
        $pedagogicalReferent->setEnabled(true);

        if ($pedagogicalReferent->getSuperAdmin()) {
            $pedagogicalReferent->setRoles(['ROLE_SUPER_ADMIN']);
        } else {
            $pedagogicalReferent->setRoles(['ROLE_ADMIN']);
        }

        return $this->save($pedagogicalReferent);
    }

}
