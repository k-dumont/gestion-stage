<?php

namespace AppBundle\Manager;

use AppBundle\Entity\ClassRoom;
use AppBundle\Entity\Inscription;

/**
 * Class ClassRoomManager
 * @package AppBundle\Manager
 */
class ClassRoomManager extends GlobalEntityManager
{

    protected $entityClass = ClassRoom::class;

    /**
     * @param ClassRoom $classRoom
     * @return array
     */
    public function getStudentByClassRoom(ClassRoom $classRoom)
    {
        $studentList = [];

        /** @var Inscription $inscription */
        foreach ($classRoom->getInscriptions() as $inscription) {
            $studentList[] = $inscription->getStudent();
        }

        return $studentList;
    }

}
