<?php

namespace AppBundle\Manager;

use AppBundle\Entity\ConnexionLog;
use AppBundle\Repository\ConnexionLogRepository;

/**
 * Class ConnexionLogManager
 * @package AppBundle\Manager
 */
class ConnexionLogManager extends GlobalEntityManager
{
    /**
     * @var
     */
    protected $entityClass = ConnexionLog::class;

    /**
     * @param ConnexionLog $connexionLog
     * @return $this
     */
    public function add(ConnexionLog $connexionLog)
    {

        if ($connexionLog && $this->em->isOpen()) {
            $this->save($connexionLog);
        }

        return $this;
    }

}
