<?php

namespace AppBundle\Manager;

use AppBundle\Entity\Stage;
use AppBundle\Entity\Student;
use AppBundle\Repository\StageRepository;

/**
 * Class StageManager
 * @package AppBundle\Manager
 */
class StageManager extends GlobalEntityManager
{
    /**
     * @var Stage
     */
    protected $entityClass = Stage::class;

    /**
     * @param Stage $stage
     * @param Student $student
     * @return bool
     */
    public function addStage(Stage $stage, Student $student)
    {
        if ($stage && $student->getId()) {
            $stage->setStudent($student);

            return $this->save($stage);
        }

        return false;
    }
}
