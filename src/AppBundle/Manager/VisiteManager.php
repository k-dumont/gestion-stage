<?php

namespace AppBundle\Manager;

use AppBundle\Entity\Stage;
use AppBundle\Entity\Visite;

/**
 * Class VisiteManager
 * @package AppBundle\Manager
 */
class VisiteManager extends GlobalEntityManager
{
    /**
     * Visite
     */
    protected $entityClass = Visite::class;

    /**
     * @param Visite $visite
     * @param Stage $stage
     * @return bool
     */
    public function addVisite(Visite $visite, Stage $stage)
    {
        if (!$visite || !$stage->getId()) {
            return false;
        }

        $visite->setStage($stage);
        return $this->save($visite);
    }
}
