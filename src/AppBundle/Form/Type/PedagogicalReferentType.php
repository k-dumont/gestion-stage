<?php

namespace AppBundle\Form\Type;

use AppBundle\Entity\PedagogicalReferent;
use AppBundle\Traits\Forms\ContactFieldsTrait;
use AppBundle\Traits\Forms\NamesFieldsTrait;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;

class PedagogicalReferentType extends AbstractType
{
    /**
     * Get Names and contact fields
     */
    use NamesFieldsTrait {
        nameBuildForm as private namesFormBuilder;
    }

    use ContactFieldsTrait {
        contactBuildForm as private contactFormBuilder;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /**
         * Get first name and last name fields
         */
        $this->namesFormBuilder($builder);

        /**
         * Get contact fields
         */
        $this->contactFormBuilder($builder);

        /** @var PedagogicalReferent $pedagogicalReferent */
        $pedagogicalReferent = $builder->getData();

        $builder
            ->add('plainPassword', RepeatedType::class, array(
                'type' => PasswordType::class,
                'invalid_message' => 'Les mot de passe saisie doivent correspondre',
                'options' => array('attr' => array('class' => 'password-field')),
                'first_options' => array('label' => 'Mot de passe'),
                'second_options' => array('label' => 'Confirmation'),
                'required' => !$pedagogicalReferent->getId(),
            ))
            ->add('superAdmin', CheckboxType::class, [
                'required' => false,
                'label' => 'Est un super administrateur',
                'attr' => ['checked' =>  $pedagogicalReferent->hasRole('ROLE_SUPER_ADMIN') ? 'checked' : false],
            ])
            ->add('Valider', SubmitType::class);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => PedagogicalReferent::class,
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_pedagogicalreferent';
    }
}
