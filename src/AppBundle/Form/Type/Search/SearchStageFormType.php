<?php

namespace AppBundle\Form\Type\Search;

use AppBundle\Entity\ClassRoom;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class SearchStageFormType
 * @package AppBundle\Form\Type\Search
 */
class SearchStageFormType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('classroom', EntityType::class, [
            'class' => 'AppBundle:ClassRoom',
            'query_builder' => function (EntityRepository $er) {
                return $er->createQueryBuilder('cr')
                    ->orderBy('cr.classRoomLabel', 'ASC');
            },
            'choice_label' => 'classRoomLabel',
            'label' => 'Classes',
            'required' => true,
        ]);
    }

    /**
     * @return null
     */
    public function getBlockPrefix()
    {
        return null;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => ClassRoom::class,
            'csrf_protection' => false,
        ));
    }

}
