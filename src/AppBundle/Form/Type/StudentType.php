<?php

namespace AppBundle\Form\Type;

use AppBundle\Entity\Student;
use AppBundle\Traits\Forms\AddressFieldsTrait;
use AppBundle\Traits\Forms\ContactFieldsTrait;
use AppBundle\Traits\Forms\NamesFieldsTrait;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class StudentType extends AbstractType
{
    /**
     * Get Names, Contacts, Address fields
     */
    use NamesFieldsTrait {
        nameBuildForm as private nameFormBuilder;
    }
    use ContactFieldsTrait {
        contactBuildForm as private contactFormBuilder;
    }
    use AddressFieldsTrait {
        addressBuildForm as private addressFormBuilder;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /**
         * Get first name and last name fields
         */
        $this->nameFormBuilder($builder);

        /**
         * Get contact fields
         */
        $this->contactFormBuilder($builder);

        /**
         * Get address fields
         */
        $this->addressFormBuilder($builder);

        $builder->add('bacalaureat', EntityType::class, [
            'class' => 'AppBundle:Bacalaureat',
            'query_builder' => function (EntityRepository $er) {
                return $er->createQueryBuilder('b')
                    ->orderBy('b.bacLabel', 'ASC');
            },
            'choice_label' => 'bacLabel',
            'label' => 'Bacalaureat',
            'required' => true,
        ]);

        $builder->add('bacGetting', DateType::class, [
            'required' => true,
            'label' => 'Année obtention BAC'
        ]);

        $builder->add('Valider', SubmitType::class);


    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Student::class,
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_student';
    }


}
