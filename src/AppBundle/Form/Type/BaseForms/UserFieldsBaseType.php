<?php

namespace AppBundle\Form\Type\BaseForms;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserFieldsBaseType extends AbstractType
{
    /**
     * @var string
     * Set Data class
     */
    protected $dataClass;

    /**
     * @var string
     * Set Block prefix
     */
    protected $blockPrefix;

    /**
     * @var boolean
     * Add contact fields true/false
     */
    protected $addContectFields;

    /**
     * @var boolean
     * Add address fields true/false
     */
    protected $addAddressFields;

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('firstName', TextType::class, [
            'required' => true,
            'label' => 'Prénom',
        ]);

        $builder->add('lastName', TextType::class, [
            'required' => true,
            'label' => 'Nom',
        ]);

        if ($this->addContectFields === true) {

            $builder->add('phone', IntegerType::class, [
                'required' => true,
                'label' => 'Tél'
            ]);

            $builder->add('email', EmailType::class, [
                'required' => true,
                'label' => 'Email'
            ]);
        }

        if ($this->addAddressFields === true) {

            $builder->add('streetNumber', TextType::class, [
                'required' => true,
                'label' => 'N°',
            ]);

            $builder->add('streetLabel', TextType::class, [
                'required' => true,
                'label' => 'Rue',
            ]);

            $builder->add('zipCode', IntegerType::class, [
                'required' => true,
                'label' => 'Code Postal',
            ]);

            $builder->add('city', TextType::class, [
                'required' => true,
                'label' => 'Ville',
            ]);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => $this->dataClass,
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return $this->blockPrefix;
    }


}

