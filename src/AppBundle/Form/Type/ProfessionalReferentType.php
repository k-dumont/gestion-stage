<?php

namespace AppBundle\Form\Type;

use AppBundle\Traits\Forms\NamesFieldsTrait;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class ProfessionalReferentType
 * @package AppBundle\Form\Type
 */
class ProfessionalReferentType extends AbstractType
{
    /**
     * Get first name and last name fields
     */
    use NamesFieldsTrait {
        nameBuildForm as private namesFormBuilder;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /**
         * Get first name and last name fields
         */
        $this->namesFormBuilder($builder);

        $builder->add('professionalReferentFonction', ChoiceType::class, [
            'required' => true,
            'label' => 'Fonction',
            'placeholder' => 'Selectionner une fonction',
            'choices' => [
                'Integrateur' => 'Integrateur',
                'Développeur' => 'Développeur',
                'Chef de projet' => 'Chef de projet',
            ]
        ]);

        $builder->add('Valider', SubmitType::class);

    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\ProfessionalReferent'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_professionalreferent';
    }


}
