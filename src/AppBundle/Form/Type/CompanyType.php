<?php

namespace AppBundle\Form\Type;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Doctrine\ORM\EntityRepository;
use AppBundle\Traits\Forms\AddressFieldsTrait;

class CompanyType extends AbstractType
{
    /**
     * Get Address fields
     */
    use AddressFieldsTrait {
        addressBuildForm as private addressFormBuilder;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('name', TextType::class, [
            'label' => 'Nom',
            'required' => true,
        ]);

        $builder->add('turnover', TextType::class, [
            'label' => 'Chiffre d\'affaires',
            'required' => true,
        ]);

        $builder->add('phone', TextType::class, [
            'label' => 'Tél',
            'required' => true,
        ]);

        $builder->add('professionalReferent', EntityType::class, [
            'class' => 'AppBundle:ProfessionalReferent',
            'query_builder' => function (EntityRepository $er) {
                return $er->createQueryBuilder('pr')
                    ->orderBy('pr.lastName', 'ASC');
            },
            'choice_label' => 'lastName',
            'label' => 'Réferant',
            'required' => true,
            'placeholder' => "Veuillez choisir un référent professionnel"
        ]);

        $builder->add('companyType', EntityType::class, [
            'class' => 'AppBundle:CompanyType',
            'query_builder' => function (EntityRepository $er) {
                return $er->createQueryBuilder('cp')
                    ->orderBy('cp.typeLabel', 'ASC');
            },
            'choice_label' => 'typeLabel',
            'label' => 'Type entreprise',
            'required' => true,
        ]);

        /**
         * Address fields
         */
        $this->addressFormBuilder($builder);

        $builder->add('Valider', SubmitType::class);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Company'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_company';
    }


}
