<?php

namespace AppBundle\Form\Type;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Doctrine\ORM\EntityRepository;

class ClassRoomType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('classRoomLabel', TextType::class, [
            'required' => true,
            'label' => 'Classe'
        ]);
        $builder->add('designation', TextType::class, [
            'required' => true,
            'label' => 'Désignation',
        ]);
        $builder->add('promo', EntityType::class, [
            'class' => 'AppBundle:Promo',
            'query_builder' => function (EntityRepository $er) {
                return $er->createQueryBuilder('p')
                    ->orderBy('p.promoLabel', 'ASC');
            },
            'choice_label' => 'promoLabel',
            'label' => 'Promo',
            'required' => true,
            'placeholder' => "Veuillez choisir une promo",
        ]);

        $builder->add('Valider', SubmitType::class);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\ClassRoom'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_classroom';
    }


}
