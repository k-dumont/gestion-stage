<?php

namespace AppBundle\Form\Type;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Doctrine\ORM\EntityRepository;

class InscriptionType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('promo', EntityType::class, [
            'class' => 'AppBundle:Promo',
            'query_builder' => function (EntityRepository $er) {
                return $er->createQueryBuilder('p')
                    ->orderBy('p.promoLabel', 'ASC');
            },
            'choice_label' => 'promoLabel',
            'label' => 'Promo',
            'required' => true,
        ]);

        $builder->add('classRoom', EntityType::class, [
            'class' => 'AppBundle:ClassRoom',
            'query_builder' => function (EntityRepository $er) {
                return $er->createQueryBuilder('cr')
                    ->orderBy('cr.classRoomLabel', 'ASC');
            },
            'choice_label' => 'classRoomLabel',
            'label' => 'Classe',
            'required' => true,
        ]);

        $builder->add('Valider', SubmitType::class);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Inscription'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_inscription';
    }


}
