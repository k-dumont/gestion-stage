<?php

namespace AppBundle\Twig;

use AppBundle\Entity\Inscription;
use AppBundle\Entity\Student;
use AppBundle\Manager\GlobalEntityManager;
use AppBundle\Repository\InscriptionRepository;

/**
 * Class StageExtension
 * @package AppBundle\Twig
 */
class StageExtension extends \Twig_Extension
{

    /**
     * @var GlobalEntityManager
     */
    private $globalEntityManager;

    /**
     * StageExtension constructor.
     * @param GlobalEntityManager $globalEntityManager
     */
    public function __construct(GlobalEntityManager $globalEntityManager)
    {
        $this->globalEntityManager = $globalEntityManager;
    }

    /**
     * @return array
     */
    public function getFunctions()
    {
        return [
            new \Twig_SimpleFunction('getClassRoom', array($this, 'getClassRoom'))
        ];
    }

    /**
     * @param Student $student
     * @return string
     */
    public function getClassRoom(Student $student)
    {
        $studentInscriptionManager = $this->globalEntityManager->setEntityClass(Inscription::class);
        $studentInscription = $studentInscriptionManager->getRepository()->findOneBy(['student' => $student]);

        return $studentInscription->getClassRoom()->getClassRoomLabel();
    }
}

