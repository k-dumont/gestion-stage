<?php

namespace AppBundle\Traits\Forms;

use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\FormBuilderInterface;

/**
 * Class ContactFieldsTrait
 * @package AppBundle\Traits\Forms
 */
trait ContactFieldsTrait
{
    /**
     * @param $builder
     */
    public function contactBuildForm(FormBuilderInterface $builder)
    {
        $builder->add('phone', IntegerType::class, [
            'required' => true,
            'label' => 'Tél'
        ]);

        $builder->add('email', EmailType::class, [
            'required' => true,
            'label' => 'Email'
        ]);
    }
}
