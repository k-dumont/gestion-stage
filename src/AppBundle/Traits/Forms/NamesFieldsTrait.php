<?php

namespace AppBundle\Traits\Forms;

use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

/**
 * Class NamesFieldsTrait
 * @package AppBundle\Traits\Forms
 */
trait NamesFieldsTrait
{
    /**
     * @param $builder
     */
    public function nameBuildForm(FormBuilderInterface $builder)
    {
        $builder->add('firstName', TextType::class, [
            'required' => true,
            'label' => 'Prénom',
        ]);

        $builder->add('lastName', TextType::class, [
            'required' => true,
            'label' => 'Nom',
        ]);
    }
}
