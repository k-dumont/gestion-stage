<?php

namespace AppBundle\Traits\Forms;

use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\FormBuilderInterface;

/**
 * Class AddressFieldsTrait
 * @package AppBundle\Traits\Forms
 */
trait AddressFieldsTrait
{
    /**
     * @param $builder
     */
    public function addressBuildForm(FormBuilderInterface $builder)
    {
        $builder->add('streetNumber', TextType::class, [
            'required' => true,
            'label' => 'N°',
        ]);

        $builder->add('streetLabel', TextType::class, [
            'required' => true,
            'label' => 'Rue',
        ]);

        $builder->add('zipCode', IntegerType::class, [
            'required' => true,
            'label' => 'Code Postal',
        ]);

        $builder->add('city', TextType::class, [
            'required' => true,
            'label' => 'Ville',
        ]);

    }
}
