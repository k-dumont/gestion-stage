<?php

namespace AppBundle\Services;

use Symfony\Component\HttpFoundation\Session\SessionInterface;

/**
 * Class NotificationsService
 * @package AppBundle\Services
 */
class NotificationsService
{
    const STANDARD_ADD_MSG = 'Ajout effectué avec succès';
    const STANDARD_REMOVE_MSG = 'Suppression effectuée avec succès';
    const STANDARD_EDIT_MSG = 'Modification effectuée avec succès';
    const STANDARD_WARNING_MSG = 'Une erreur inconnue est survenue, veuillez réessayer plus tard';

    /**
     * @var SessionInterface
     */
    private $session;

    /**
     * NotificationsService constructor.
     * @param SessionInterface $session
     */
    public function __construct(SessionInterface $session)
    {
        $this->session = $session;
    }

    /**
     * Success notification
     */
    public function success($msg)
    {
        return $this->session->getBag('flashes')->add('success', $msg);
    }


    /**
     * Warning notification
     */
    public function warning($msg)
    {
        return $this->session->getBag('flashes')->add('warning', $msg);
    }

    /**
     * Success Add standard
     */
    public function successAdd()
    {
        $this->success(self::STANDARD_ADD_MSG);
    }

    /**
     * Success remove standard
     */
    public function successRemove()
    {
        $this->success(self::STANDARD_REMOVE_MSG);
    }

    /**
     * Success edit standard
     */
    public function successEdit()
    {
        $this->success(self::STANDARD_EDIT_MSG);
    }

    /**
     * Success Add standard
     */
    public function warningStandard()
    {
        $this->warning(self::STANDARD_WARNING_MSG);
    }

}

