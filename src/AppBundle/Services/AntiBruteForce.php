<?php

namespace AppBundle\Services;

use AppBundle\Entity\ConnexionLog;
use AppBundle\Manager\ConnexionLogManager;
use AppBundle\Repository\ConnexionLogRepository;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Routing\RouterInterface;

/**
 * Class AntiBruteForce
 * @package AppBundle\Services
 */
class AntiBruteForce
{

    const DELAY_BRUTE_FORCE = 1;

    const MAX_CONNECTION_PER_DELAY = 3;

    /**
     * @var RequestStack
     */
    private $requestStack;

    /**
     * @var ConnexionLogManager
     */
    private $connexionLogManager;

    /**
     * AntiBruteForce constructor.
     * @param RequestStack $requestStack
     * @param RouterInterface $router
     * @param ConnexionLogManager $connexionLogManager
     */
    public function __construct(
        RequestStack $requestStack,
        ConnexionLogManager $connexionLogManager
    )
    {
        $this->requestStack = $requestStack;
        $this->connexionLogManager = $connexionLogManager;
    }

    /**
     * @return $this
     */
    public function log()
    {
        $connexionLog = new ConnexionLog();
        $connexionLog->setClientIp($this->requestStack->getCurrentRequest()->getClientIp());
        $connexionLog->setConnexionDatetime(new \DateTime());

        $this->connexionLogManager->add($connexionLog);

        return $this;
    }

    /**
     * @return bool
     */
    public function isBruteForceRequest()
    {
        $date = new \DateTime();
        $date->modify('-' . self::DELAY_BRUTE_FORCE . ' hour');

        /** @var ConnexionLogRepository $repository */
        $repository = $this->connexionLogManager->getRepository();

        $connexions = [];

        if (!is_null($repository)) {

            $connexions = $repository
                ->getConnectionByIpAndByDatetime($this->requestStack->getCurrentRequest()->getClientIp(), $date)
                ->setMaxResults(self::MAX_CONNECTION_PER_DELAY)
                ->getQuery()
                ->getResult();
        }

        return count($connexions) >= self::MAX_CONNECTION_PER_DELAY;
    }
}
