<?php

namespace AppBundle\Controller\Company;

use AppBundle\Entity\CompanyType;
use AppBundle\Form\Type\CompanyTypeType;
use AppBundle\Manager\GlobalEntityManager;
use AppBundle\Services\NotificationsService;
use Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class CompanyTypeController extends Controller
{
    /**
     * NotificationsService
     */
    private $notificationsService;

    /**
     * @var GlobalEntityManager
     */
    private $companyTypeManager;

    /**
     * CompanyTypeController constructor.
     * @param GlobalEntityManager $globalEntityManager
     * @param NotificationsService $notificationsService
     */
    public function __construct(GlobalEntityManager $globalEntityManager, NotificationsService $notificationsService)
    {
        $this->notificationsService = $notificationsService;
        $this->companyTypeManager = $globalEntityManager->setEntityClass(CompanyType::class);
    }

    /**
     * @Route("/", name="company_type_home", methods={"GET"})
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function homeCompanyTypeAction()
    {
        return $this->render('company_type/company_type_home.html.twig', [
            'elements' => $this->companyTypeManager->getRepository()->findAll(),
        ]);
    }

    /**
     * @Route("/add", name="company_type_add", methods={"GET","POST"})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function addCompanyTypeAction(Request $request)
    {
        $companyType = new CompanyType();
        $form = $this->createForm(CompanyTypeType::class, $companyType);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            if ($this->companyTypeManager->save($companyType)) {
                $this->notificationsService->successAdd();
            } else {
                $this->notificationsService->warningStandard();
            }

            return $this->redirect($this->generateUrl('company_type_home'));
        }

        return $this->render('company_type/company_type_add-edit.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/edit/{id}", name="company_type_edit", methods={"GET","POST"})
     * @param Request $request
     * @param CompanyType $companyType
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function editCompanyTypeAction(Request $request, CompanyType $companyType)
    {
        $form = $this->createForm(CompanyTypeType::class, $companyType);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            if ($this->companyTypeManager->save($companyType)) {
                $this->notificationsService->successEdit();
            } else {
                $this->notificationsService->warningStandard();
            }

            return $this->redirect($request->headers->get('referer'));
        }

        return $this->render('company_type/company_type_add-edit.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/remove/{id}", name="company_type_remove", methods={"GET","POST"})
     * @param CompanyType $companyType
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function removeCompanyTypeAction(CompanyType $companyType)
    {
        try {
            if ($this->companyTypeManager->remove($companyType)) {
                $this->notificationsService->successRemove();
            } else {
                $this->notificationsService->warningStandard();
            }

        } catch (ForeignKeyConstraintViolationException $e) {
            $this->notificationsService
                ->warning('Ce type "' . $companyType->getTypeLabel() . '" ne peut pas être supprimé car il est déjà utilisé.');
        }

        return $this->redirect($this->generateUrl('company_type_home'));
    }
}
