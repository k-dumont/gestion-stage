<?php

namespace AppBundle\Controller\Company;

use AppBundle\Entity\ProfessionalReferent;
use AppBundle\Form\Type\ProfessionalReferentType;
use AppBundle\Manager\GlobalEntityManager;
use AppBundle\Services\NotificationsService;
use Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class ProfessionalReferentController extends Controller
{
    /**
     * NotificationsService
     */
    private $notificationsService;

    /**
     * @var GlobalEntityManager
     */
    private $professionalReferentManager;

    /**
     * ProfessionalReferentController constructor.
     * @param GlobalEntityManager $globalEntityManager
     * @param NotificationsService $notificationsService
     */
    public function __construct(GlobalEntityManager $globalEntityManager, NotificationsService $notificationsService)
    {
        $this->professionalReferentManager = $globalEntityManager->setEntityClass(ProfessionalReferent::class);
        $this->notificationsService = $notificationsService;
    }

    /**
     * @Route("/", name="professional_referent_home", methods={"GET"})
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function homeProfessionalReferentAction()
    {
        return $this->render('professional_referent/professional_referent_home.html.twig', [
            'elements' => $this->professionalReferentManager->getRepository()->findAll(),
        ]);
    }

    /**
     * @Route("/add/", name="professional_referent_add", methods={"GET","POST"})
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function addProfessionalReferentAction(Request $request)
    {
        $professionalReferent = new ProfessionalReferent();
        $form = $this->createForm(ProfessionalReferentType::class, $professionalReferent);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            if ($this->professionalReferentManager->actionBehavior(['firstName' => $professionalReferent->getFirstName(), 'lastName' => $professionalReferent->getLastName()])) {
                $this->notificationsService->warning('Ce référent professionnel existe déjà');
                return $this->redirect($request->headers->get('referer'));
            }

            if ($this->professionalReferentManager->save($professionalReferent)) {
                $this->notificationsService->successAdd();
            } else {
                $this->notificationsService->warningStandard();
            }

            return $this->redirect($this->generateUrl('professional_referent_home'));
        }

        return $this->render('professional_referent/professional_referent_add-edit.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/edit/{id}/", name="professional_referent_edit", methods={"GET","POST"})
     *
     * @param Request $request
     * @param ProfessionalReferent $professionalReferent
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function editProfessionalReferentAction(Request $request, ProfessionalReferent $professionalReferent)
    {
        $form = $this->createForm(ProfessionalReferentType::class, $professionalReferent);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            if ($this->professionalReferentManager->save($professionalReferent)) {
                $this->notificationsService->successEdit();
            } else {
                $this->notificationsService->warningStandard();
            }

            return $this->redirect($request->headers->get('referer'));
        }

        return $this->render('professional_referent/professional_referent_add-edit.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/remove/{id}/", name="professional_referent_remove", methods={"GET","POST"})
     *
     * @param Request $request
     * @param ProfessionalReferent $professionalReferent
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function removeProfessionalReferentAction(Request $request, ProfessionalReferent $professionalReferent)
    {
        if (!$professionalReferent->getId()) {
            $this->notificationsService->warningStandard();
            return $this->redirect($this->generateUrl('professional_referent_home'));
        }

        try {

            if ($this->professionalReferentManager->remove($professionalReferent)) {
                $this->notificationsService->successRemove();
            } else {
                $this->notificationsService->warningStandard();
            }

        } catch (ForeignKeyConstraintViolationException $e) {
            $this->notificationsService->warning('Le référent "' .
                $professionalReferent->getFirstName() . ' ' .
                $professionalReferent->getLastName() . '" ne peut pas étre supprimer.');
        }

        return $this->redirect($this->generateUrl('professional_referent_home'));
    }
}
