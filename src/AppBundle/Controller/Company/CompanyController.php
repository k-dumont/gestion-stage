<?php

namespace AppBundle\Controller\Company;

use AppBundle\Entity\Company;
use AppBundle\Form\Type\CompanyType as CompanyFormType;
use AppBundle\Manager\GlobalEntityManager;
use AppBundle\Services\NotificationsService;
use Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class CompanyController extends Controller
{

    /**
     * NotificationsService
     */
    private $notificationService;

    /**
     * @var $this
     */
    private $companyManager;

    /**
     * CompanyController constructor.
     * @param GlobalEntityManager $globalEntityManager
     * @param NotificationsService $notificationsService
     */
    public function __construct(GlobalEntityManager $globalEntityManager, NotificationsService $notificationsService)
    {
        $this->notificationService = $notificationsService;
        $this->companyManager = $globalEntityManager->setEntityClass(Company::class);
    }

    /**
     * @Route("/", name="company_home", methods={"GET"})
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function homeCompanyAction()
    {
        return $this->render('company/home_company.html.twig', [
            'companies' => $this->companyManager->getRepository()->findAll(),
        ]);
    }

    /**
     * @Route("/view/{id}", name="company_view", methods={"GET"})
     *
     * @param Company $company
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function viewCompanyAction(Company $company)
    {
        return $this->render('company/view_company.html.twig', [
            'company' => $company,
        ]);
    }

    /**
     * @Route("/add", name="company_add", methods={"GET","POST"})
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function addCompanyAction(Request $request)
    {
        $company = new Company();
        $form = $this->createForm(CompanyFormType::class, $company);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            if ($this->companyManager->actionBehavior(['name' => $company->getName()])) {
                $this->notificationService->warning('L\'entreprise existe déjà');

                return $this->redirect($request->headers->get('referer'));
            }

            if ($this->companyManager->save($company)) {
                $this->notificationService->successAdd();
            } else {
                $this->notificationService->warningStandard();
            }

            return $this->redirect($this->generateUrl('company_home'));
        }

        return $this->render('company/company_add-edit.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/edit/{id}/", name="company_edit", methods={"GET","POST"})
     *
     * @param Request $request
     * @param Company $company
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function editCompanyAction(Request $request, Company $company)
    {
        $form = $this->createForm(CompanyFormType::class, $company);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            if ($this->companyManager->save($company)) {
                $this->notificationService->successEdit();
            } else {
                $this->notificationService->warningStandard();
            }

            return $this->redirect($request->headers->get('referer'));
        }

        return $this->render('company/company_add-edit.html.twig', [
            'form' => $form->createView(),
            'editMode' => true,
            'company' => $company
        ]);
    }

    /**
     * @Route("/remove/{id}/", name="company_remove", methods={"GET","POST"})
     *
     * @param Company $company
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function removeCompanyAction(Company $company)
    {
        try {

            if ($this->companyManager->remove($company)) {
                $this->notificationService->successRemove();
            } else {
                $this->notificationService->warningStandard();
            }

        } catch (ForeignKeyConstraintViolationException $e) {
            $this->notificationService->warning('L\'entreprise "' . $company->getName() . ' ne peut pas étre supprimer.');
        }

        return $this->redirect($this->generateUrl('company_home'));
    }
}
