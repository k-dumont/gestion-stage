<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Technologie;
use AppBundle\Form\Type\TechnologieType;
use AppBundle\Manager\GlobalEntityManager;
use AppBundle\Services\NotificationsService;
use Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;


class TechnologieController extends Controller
{
    /**
     * @var $this
     */
    private $technologieManager;

    /**
     * NotificationsService
     */
    private $notificationsService;

    /**
     * TechnologieController constructor.
     * @param GlobalEntityManager $globalEntityManager
     * @param NotificationsService $notificationsService
     */
    public function __construct(GlobalEntityManager $globalEntityManager, NotificationsService $notificationsService)
    {
        $this->technologieManager = $globalEntityManager->setEntityClass(Technologie::class);
        $this->notificationsService = $notificationsService;
    }

    /**
     * @Route("/", name="technologie_home", methods={"GET"})
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function homeTechnologieAction()
    {
        return $this->render('technologie/home.html.twig', [
            'elements' => $this->technologieManager->getRepository()->findAll(),
        ]);
    }

    /**
     * @Route("/add/", name="technologie_add", methods={"GET","POST"})
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function addTechnologieAction(Request $request)
    {
        $technologie = new Technologie();
        $form = $this->createForm(TechnologieType::class, $technologie);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            if ($this->technologieManager->save($technologie)) {
                $this->notificationsService->successAdd();
            } else {
                $this->notificationsService->warningStandard();
            }

            return $this->redirect($this->generateUrl('technologie_home'));
        }

        return $this->render('technologie/add-edit.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/edit/{id}/", name="technologie_edit", methods={"GET","POST"})
     *
     * @param Request $request
     * @param Technologie $technologie
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function editTechnologieAction(Request $request, Technologie $technologie)
    {
        $form = $this->createForm(TechnologieType::class, $technologie);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            if ($this->technologieManager->save($technologie)) {
                $this->notificationsService->successEdit();
            } else {
                $this->notificationsService->warningStandard();
            }

            return $this->redirect($request->headers->get('referer'));
        }

        return $this->render('technologie/add-edit.html.twig', [
            'form' => $form->createView(),
            'editMode' => true,
        ]);
    }

    /**
     * @Route("/remove/{id}/", name="technologie_remove", methods={"GET","POST"})
     *
     * @param Technologie $technologie
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function removeTechnologieAction(Technologie $technologie)
    {
        try {

            if ($this->technologieManager->remove($technologie)) {
                $this->notificationsService->successRemove();
            } else {
                $this->notificationsService->warningStandard();
            }

        } catch (ForeignKeyConstraintViolationException $e) {
            $this->notificationsService->warning('Le technologie "' .
                $technologie->getTechnologieLabel() . '" ne peut pas être supprimée.');
        }

        return $this->redirect($this->generateUrl('technologie_home'));
    }
}
