<?php

namespace AppBundle\Controller\Schooling;

use AppBundle\Entity\ClassRoom;
use AppBundle\Entity\Promo;
use AppBundle\Form\Type\ClassRoomType;
use AppBundle\Form\Type\PromoType;
use AppBundle\Manager\GlobalEntityManager;
use AppBundle\Services\NotificationsService;
use Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;

class ClassRoomController extends Controller
{
    /**
     * @var NotificationsService
     */
    private $notificationsService;

    /**
     * @var $this
     */
    private $classRoomManager;

    /**
     * @var $this
     */
    private $promoManager;

    /**
     * ClassRoomController constructor.
     * @param NotificationsService $notificationsService
     * @param GlobalEntityManager $globalEntityManager
     */
    public function __construct(NotificationsService $notificationsService, GlobalEntityManager $globalEntityManager)
    {
        $this->notificationsService = $notificationsService;

        $this->classRoomManager = clone $globalEntityManager;
        $this->classRoomManager->setEntityClass(ClassRoom::class);

        $this->promoManager = clone $globalEntityManager;
        $this->promoManager->setEntityClass(Promo::class);
    }

    /**
     * @Route("/", name="schooling_classroom", methods={"GET"})
     */
    public function indexAction()
    {
        return $this->render('schooling/classroom/index.html.twig', [
            'elements' => $this->classRoomManager->getRepository()->findAll(),
        ]);
    }

    /**
     * @Route("/new/", name="schooling_classroom_new", methods={"GET","POST"})
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function newClassRoomAction(Request $request)
    {
        $classRoom = new ClassRoom();
        $form = $this->createForm(ClassRoomType::class, $classRoom);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            if ($this->classRoomManager->save($classRoom)) {
                $this->notificationsService->successAdd();
            } else {
                $this->notificationsService->warningStandard();
            }

            return $this->redirect($this->generateUrl('schooling_classroom'));
        }

        return $this->render('schooling/classroom/add-edit.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/edit/{id}/", name="schooling_classroom_edit", methods={"GET","POST"})
     *
     * @param Request $request
     * @param ClassRoom $classRoom
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function editClassRoomAction(Request $request, ClassRoom $classRoom)
    {
        $form = $this->createForm(ClassRoomType::class, $classRoom);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            try {
                if ($this->classRoomManager->save($classRoom)) {
                    $this->notificationsService->successAdd();
                } else {
                    $this->notificationsService->warningStandard();
                }
            } catch (UniqueConstraintViolationException $e) {
                $this->notificationsService->warning('La promo est déjà affectée ');
            }

            return $this->redirect($request->headers->get('referer'));
        }

        return $this->render('schooling/classroom/add-edit.html.twig', [
            'form' => $form->createView(),
            'editMode' => true,
        ]);
    }

    /**
     * @Route("/remove/{id}/", name="schooling_classroom_remove", methods={"GET","POST"})
     *
     * @param ClassRoom $classRoom
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function removeClassRoomAction(ClassRoom $classRoom)
    {
        try {

            if ($this->classRoomManager->remove($classRoom)) {
                $this->notificationsService->successRemove();
            } else {
                $this->notificationsService->warningStandard();
            }

        } catch (ForeignKeyConstraintViolationException $e) {
            $this->notificationsService->warning('Cet classe "' . $classRoom->getClassRoomLabel() . '" ne peut pas étre supprimer.');
        }

        return $this->redirect($this->generateUrl('schooling_classroom'));
    }

    /**
     * @Route("/promo/", name="schooling_classroom_promo_new", methods={"GET","POST"})
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function newPromoAction(Request $request)
    {
        $promo = new Promo();
        $form = $this->createForm(PromoType::class, $promo);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            if ($this->promoManager->save($promo)) {
                $this->notificationsService->successAdd();
            } else {
                $this->notificationsService->warningStandard();
            }

            return $this->redirect($request->headers->get('referer'));
        }

        $query = $this->promoManager->getRepository()->findAll();

        return $this->render('schooling/classroom/promo/home_add_edit.html.twig', [
            'elements' => $query,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/promo/edit/{id}/", name="schooling_classroom_promo_edit", methods={"GET","POST"})
     *
     * @param Request $request
     * @param Promo $promo
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function editPromoAction(Request $request, Promo $promo)
    {
        $form = $this->createForm(PromoType::class, $promo);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            if ($this->promoManager->save($promo)) {
                $this->notificationsService->successEdit();
            } else {
                $this->notificationsService->warningStandard();
            }

            return $this->redirect($request->headers->get('referer'));
        }

        return $this->render('schooling/classroom/promo/home_add_edit.html.twig', [
            'form' => $form->createView(),
            'editMode' => true,
            'elements' => $this->promoManager->getRepository()->findAll(),
        ]);
    }

    /**
     * @Route("/promo/remove/{id}/", name="schooling_classroom_promo_remove", methods={"GET","POST"})
     *
     * @param Promo $promo
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function removePromoAction(Promo $promo)
    {
        try {
            if ($this->promoManager->remove($promo)) {
                $this->notificationsService->successRemove();
            } else {
                $this->notificationsService->warningStandard();
            }

        } catch (ForeignKeyConstraintViolationException $e) {
            $this->notificationsService->warning('Cet promo "' . $promo->getPromoLabel() . '" ne peut pas étre supprimer.');
        }

        return $this->redirect($this->generateUrl('schooling_classroom_promo_new'));
    }
}
