<?php

namespace AppBundle\Controller\Schooling;

use AppBundle\Entity\PedagogicalReferent;
use AppBundle\Form\Type\PedagogicalReferentType;
use AppBundle\Manager\PedagogicalReferentManager;
use AppBundle\Services\NotificationsService;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class PedagogicalReferentController extends Controller
{
    /**
     * @var NotificationsService
     */
    private $notificationsService;

    /**
     * @var PedagogicalReferentManager
     */
    private $pedagogicalReferentManager;

    /**
     * PedagogicalReferentController constructor.
     * @param NotificationsService $notificationsService
     * @param PedagogicalReferentManager $pedagogicalReferentManager
     */
    public function __construct(NotificationsService $notificationsService, PedagogicalReferentManager $pedagogicalReferentManager)
    {
        $this->notificationsService = $notificationsService;
        $this->pedagogicalReferentManager = $pedagogicalReferentManager;
    }

    /**
     * @Route("/", name="pedagogical_referent_home", methods={"GET"})
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function homePedagogicalReferentAction()
    {
        return $this->render('schooling/pedagogical_referent/home.html.twig', [
            'elements' => $this->pedagogicalReferentManager->getRepository()->findAll(),
        ]);
    }

    /**
     * @Route("/add/", name="pedagogical_referent_add", methods={"GET","POST"})
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function addPedagogicalReferentAction(Request $request)
    {
        $pedagogicalReferent = new PedagogicalReferent();
        $form = $this->createForm(PedagogicalReferentType::class, $pedagogicalReferent);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            if ($this->pedagogicalReferentManager->savePedagogicalReferent($pedagogicalReferent)) {
                $this->notificationsService->successAdd();
            } else {
                $this->notificationsService->warningStandard();
            }

            return $this->redirect($this->generateUrl('pedagogical_referent_home'));
        }

        return $this->render('schooling/pedagogical_referent/add-edit.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/edit/{id}/", name="pedagogical_referent_edit", methods={"GET","POST"})
     *
     * @param Request $request
     * @param PedagogicalReferent $pedagogicalReferent
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function editPedagogicalReferentAction(Request $request, PedagogicalReferent $pedagogicalReferent)
    {
        $form = $this->createForm(PedagogicalReferentType::class, $pedagogicalReferent);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            if ($this->pedagogicalReferentManager->savePedagogicalReferent($pedagogicalReferent)) {
                $this->notificationsService->successEdit();
            } else {
                $this->notificationsService->warningStandard();
            }

            return $this->redirect($request->headers->get('referer'));
        }

        return $this->render('schooling/pedagogical_referent/add-edit.html.twig', [
            'form' => $form->createView(),
            'editMode' => true,
        ]);
    }

    /**
     * @Route("/remove/{id}/", name="pedagogical_referent_remove", methods={"GET","POST"})
     *
     * @param PedagogicalReferent $pedagogicalReferent
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function removePedagogicalReferentAction(PedagogicalReferent $pedagogicalReferent)
    {
        if ($this->pedagogicalReferentManager->remove($pedagogicalReferent)) {
            $this->notificationsService->successRemove();
        } else {
            $this->notificationsService->warningStandard();
        }

        return $this->redirect($this->generateUrl('pedagogical_referent_home'));
    }
}
