<?php

namespace AppBundle\Controller\Schooling;

use AppBundle\Entity\Inscription;
use AppBundle\Entity\Student;
use AppBundle\Form\Type\InscriptionType;
use AppBundle\Form\Type\StudentType;
use AppBundle\Manager\GlobalEntityManager;
use AppBundle\Repository\InscriptionRepository;
use AppBundle\Services\NotificationsService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class StudentController extends Controller
{
    /**
     * @var NotificationsService
     */
    private $notificationsService;

    /**
     * @var GlobalEntityManager
     */
    private $studentManager;

    /**
     * @var $this
     */
    private $inscriptionManager;

    /**
     * StudentController constructor.
     * @param NotificationsService $notificationsService
     * @param GlobalEntityManager $globalEntityManager
     */
    public function __construct(NotificationsService $notificationsService, GlobalEntityManager $globalEntityManager)
    {
        $this->notificationsService = $notificationsService;

        $this->inscriptionManager = clone $globalEntityManager;
        $this->inscriptionManager->setEntityClass(Inscription::class);

        $this->studentManager = clone $globalEntityManager;
        $this->studentManager->setEntityClass(Student::class);
    }

    /**
     * @Route("/", name="schooling_student", methods={"GET"})
     */
    public function indexAction()
    {
        return $this->render('schooling/student/index.html.twig', [
            'elements' => $this->studentManager->getRepository()->findAll(),
        ]);
    }

    /**
     * @Route("/view/{id}/", name="schooling_student_view", methods={"GET"})
     *
     * @param Student $student
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function studentViewAction(Student $student)
    {
        return $this->render('schooling/student/view.html.twig', [
            'student' => $student,
            'classrooms' => $this->inscriptionManager->getRepository()->findBy(['student' => $student->getId()]),
        ]);
    }

    /**
     * @Route("/new/", name="schooling_student_new", methods={"GET","POST"})
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function newStudentAction(Request $request)
    {
        $student = new Student();
        $form = $this->createForm(StudentType::class, $student);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            if ($this->studentManager->save($student)) {
                $this->notificationsService->successAdd();
            } else {
                $this->notificationsService->warningStandard();
            }

            return $this->redirect($this->generateUrl('schooling_student'));
        }

        return $this->render('schooling/student/add-edit.html.twig', [
            'form' => $form->createView(),
        ]);

    }

    /**
     * @Route("/edit/{id}/", name="schooling_student_edit", methods={"GET","POST"})
     *
     * @param Request $request
     * @param Student $student
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function editStudentAction(Request $request, Student $student)
    {
        $form = $this->createForm(StudentType::class, $student);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            if ($this->studentManager->save($student)) {
                $this->notificationsService->successAdd();
            } else {
                $this->notificationsService->warningStandard();
            }

            return $this->redirect($this->generateUrl('schooling_student'));
        }

        return $this->render('schooling/student/add-edit.html.twig', [
            'form' => $form->createView(),
            'editMode' => true,
            'student' => $student,
        ]);
    }

    /**
     * @Route("/remove/{id}/", name="schooling_student_remove", methods={"GET","POST"})
     *
     * @param Student $student
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function removeStudentAction(Student $student)
    {
        if ($this->studentManager->remove($student)) {
            $this->notificationsService->successRemove();
        } else {
            $this->notificationsService->warningStandard();
        }

        return $this->redirect($this->generateUrl('schooling_student'));
    }

    /**
     * @Route("/classroom/{id}", name="schooling_student_classroom_new", methods={"GET","POST"})
     *
     * @param Request $request
     * @param Student $student
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function newClassRoomStudentAction(Request $request, Student $student)
    {
        $inscription = new Inscription();
        $inscription->setStudent($student);
        $form = $this->createForm(InscriptionType::class, $inscription);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            /** @var InscriptionRepository $repository */
            $repository = $this->inscriptionManager->getRepository();

            if ($repository->checkClassRoomAffectationToStudent($inscription)) {
                $this->notificationsService
                    ->warning('La promo "' . $inscription->getPromo()->getPromoLabel() . '" est déjà affectée.');

                return $this->redirect($request->headers->get('referer'));
            }

            if ($this->inscriptionManager->save($inscription)) {
                $this->notificationsService->successAdd();
            } else {
                $this->notificationsService->warningStandard();
            }

            return $this->redirect($this->generateUrl('schooling_student_edit', ['id' => $student->getId()]));
        }

        return $this->render('schooling/student/classroom/add-edit.html.twig', [
            'form' => $form->createView(),
            'student' => $student,
        ]);

    }

    /**
     * @Route("/classroom/edit/{id}/", name="schooling_student_classroom_edit", methods={"GET","POST"})
     *
     * @param Request $request
     * @param Inscription $inscription
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function editClassRoomStudentAction(Request $request, Inscription $inscription)
    {
        $form = $this->createForm(InscriptionType::class, $inscription);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            if ($this->inscriptionManager->save($inscription)) {
                $this->notificationsService->successAdd();
            } else {
                $this->notificationsService->warningStandard();
            }

            return $this->redirect($request->headers->get('referer'));
        }

        return $this->render('schooling/student/classroom/add-edit.html.twig', [
            'form' => $form->createView(),
            'student' => $inscription->getStudent(),
            'editMode' => true,
        ]);
    }

    /**
     * @Route("/classroom/remove/{id}/", name="schooling_student_classroom_remove", methods={"GET","POST"})
     *
     * @param Inscription $inscription
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function removeClassRoomStudentAction(Inscription $inscription)
    {
        if ($this->inscriptionManager->remove($inscription)) {
            $this->notificationsService->successRemove();
        } else {
            $this->notificationsService->warningStandard();
        }

        return $this->redirect($this->generateUrl('schooling_student_edit', [
            'id' => $inscription->getStudent()->getId()
        ]));
    }
}
