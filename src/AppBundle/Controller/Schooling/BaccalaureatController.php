<?php

namespace AppBundle\Controller\Schooling;

use AppBundle\Entity\Bacalaureat;
use AppBundle\Form\Type\BacalaureatType;
use AppBundle\Manager\GlobalEntityManager;
use AppBundle\Services\NotificationsService;
use Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class BaccalaureatController extends Controller
{

    /**
     * @var NotificationsService
     */
    private $notifications;

    /**
     * @var $this
     */
    private $baccalaureatManager;

    /**
     * BaccalaureatController constructor.
     * @param NotificationsService $notifications
     * @param GlobalEntityManager $globalEntityManager
     */
    public function __construct(NotificationsService $notifications, GlobalEntityManager $globalEntityManager)
    {
        $this->notifications = $notifications;
        $this->baccalaureatManager = $globalEntityManager->setEntityClass(Bacalaureat::class);
    }

    /**
     * @Route("/", name="schooling_baccalaureat", methods={"GET"})
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction()
    {
        return $this->render('schooling/baccalaureat/index.html.twig', [
            'elements' => $this->baccalaureatManager->getRepository()->findAll(),
        ]);
    }

    /**
     * @Route("/new/", name="schooling_baccalaureat_new", methods={"GET","POST"})
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function addBaccalaureatAction(Request $request)
    {
        $bacalaureat = new Bacalaureat();
        $form = $this->createForm(BacalaureatType::class, $bacalaureat);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            if ($this->baccalaureatManager->actionBehavior(['bacLabel' => $bacalaureat->getBacLabel()])) {
                $this->notifications->warning('Le bac  "' . $bacalaureat->getBacLabel() . '" existe déja');
                return $this->redirect($request->headers->get('referer'));
            }

            if ($this->baccalaureatManager->save($bacalaureat)) {
                $this->notifications->successAdd();
            } else {
                $this->notifications->warningStandard();
            }

            return $this->redirect($this->generateUrl('schooling_baccalaureat'));
        }

        return $this->render('schooling/baccalaureat/add-edit.html.twig', [
            'form' => $form->createView(),
        ]);

    }

    /**
     * @Route("/edit/{id}/", name="schooling_baccalaureat_edit", methods={"GET","POST"})
     *
     * @param Request $request
     * @param Bacalaureat $bacalaureat
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function editBaccalaureatAction(Request $request, Bacalaureat $bacalaureat)
    {
        $form = $this->createForm(BacalaureatType::class, $bacalaureat);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            if ($this->baccalaureatManager->save($bacalaureat)) {
                $this->notifications->successAdd();
            } else {
                $this->notifications->warningStandard();
            }

            return $this->redirect($request->headers->get('referer'));
        }

        return $this->render('schooling/baccalaureat/add-edit.html.twig', [
            'form' => $form->createView(),
            'editMode' => true,
        ]);
    }

    /**
     * @Route("/remove/{id}/", name="schooling_baccalaureat_remove", methods={"GET","POST"})
     *
     * @param Bacalaureat $bacalaureat
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function removeBaccalaureatAction(Bacalaureat $bacalaureat)
    {
        try {
            if ($this->baccalaureatManager->remove($bacalaureat)) {
                $this->notifications->successRemove();
            } else {
                $this->notifications->warningStandard();
            }

        } catch (ForeignKeyConstraintViolationException $e) {
            $this->notifications->warning('Le bac "' . $bacalaureat->getBacLabel() . ' ne peut pas étre supprimer.');
        }

        return $this->redirect($this->generateUrl('schooling_baccalaureat'));
    }
}
