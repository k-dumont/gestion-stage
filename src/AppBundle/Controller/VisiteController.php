<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Stage;
use AppBundle\Entity\Visite;
use AppBundle\Form\Type\VisiteType;
use AppBundle\Manager\VisiteManager;
use AppBundle\Services\NotificationsService;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class VisiteController extends Controller
{
    /**
     * @var $this
     */
    private $visiteManager;

    /**
     * NotificationsService
     */
    private $notificationsService;

    /**
     * VisiteController constructor.
     *
     * @param VisiteManager $visiteManager
     * @param NotificationsService $notificationsService
     */
    public function __construct(VisiteManager $visiteManager, NotificationsService $notificationsService)
    {
        $this->visiteManager = $visiteManager;
        $this->notificationsService = $notificationsService;
    }

    /**
     * @Route("/add/{id}", name="add_visite_to_stage", methods={"GET","POST"})
     *
     * @param Request $request
     * @param Stage $stage
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function addVisiteToStageAction(Request $request, Stage $stage)
    {
        $visite = new Visite();
        $form = $this->createForm(VisiteType::class, $visite);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            if ($this->visiteManager->addVisite($visite, $stage)) {
                $this->notificationsService->successAdd();
            } else {
                $this->notificationsService->warningStandard();
            }

            return $this->redirect($this->generateUrl('stage_view', ['id' => $stage->getId()]));
        }

        return $this->render('visite/add-edit.html.twig', [
            'form' => $form->createView(),
            'student' => $stage->getStudent(),
        ]);
    }

    /**
     * @Route("/remove/{id}", name="remove_visite_from_stage", methods={"GET","POST"})
     *
     * @param Visite $visite
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function removeVisiteAction(Visite $visite)
    {
        if ($this->visiteManager->remove($visite)) {
            $this->notificationsService->successRemove();
        } else {
            $this->notificationsService->warningStandard();
        }

        return $this->redirect($this->generateUrl('stage_view', ['id' => $visite->getStage()->getId()]));
    }
}
