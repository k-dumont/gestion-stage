<?php

namespace AppBundle\Controller;

use AppBundle\Entity\ClassRoom;
use AppBundle\Entity\Stage;
use AppBundle\Entity\Student;
use AppBundle\Form\Type\StageType;
use AppBundle\Form\Type\Search\SearchStageFormType;
use AppBundle\Manager\ClassRoomManager;
use AppBundle\Manager\GlobalEntityManager;
use AppBundle\Manager\StageManager;
use AppBundle\Repository\ClassRoomRepository;
use AppBundle\Services\NotificationsService;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

/**
 * Class StageController
 * @package AppBundle\Controller
 */
class StageController extends Controller
{
    /**
     * @var $this
     */
    private $stageManager;

    /**
     * @var GlobalEntityManager
     */
    private $studentManager;

    /**
     * ClassRoomManager
     */
    private $classRoomManager;

    /**
     * @var NotificationsService
     */
    private $notificationService;

    /**
     * @param GlobalEntityManager $globalEntityManager
     * @param NotificationsService $notificationsService
     * @param ClassRoomManager $classRoomManager
     * @param StageManager $stageManager
     */
    public function __construct(GlobalEntityManager $globalEntityManager, NotificationsService $notificationsService, ClassRoomManager $classRoomManager, StageManager $stageManager)
    {
        $this->notificationService = $notificationsService;
        $this->studentManager = clone $globalEntityManager;
        $this->studentManager->setEntityClass(Student::class);
        $this->stageManager = $stageManager;
        $this->classRoomManager = $classRoomManager;
    }

    /**
     * @Route("/", name="home_stage", methods={"GET"})
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function stageHomeAction(Request $request)
    {
        $form = $this->createForm(SearchStageFormType::class);
        $query = [];

        if ($request->get('classroom')) {
            /** @var ClassRoomRepository $repository */
            $repository = $this->classRoomManager->getRepository();

            /** @var ClassRoom $classRoom */
            $classRoom = $repository->find($request->get('classroom'));

            if ($classRoom) {
                $query = $this->classRoomManager->getStudentByClassRoom($classRoom);
            } else {
                $this->notificationService->warningStandard();
            }
        }

        return $this->render('stage/home_stage.html.twig', [
            'elements' => $query,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/view/{id}/", name="stage_view", methods={"GET"})
     *
     * @param Stage $stage
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function stageViewAction(Stage $stage)
    {
        return $this->render('stage/view_stage.html.twig', [
            'stage' => $stage,
        ]);
    }

    /**
     * @Route("/add/{id}", name="add_stage", methods={"GET","POST"})
     *
     * @param Request $request
     * @param Student $student
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function addStageAction(Request $request, Student $student)
    {
        $stage = new Stage();
        $form = $this->createForm(StageType::class, $stage);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            if ($this->stageManager->addStage($stage, $student)) {
                $this->notificationService->successAdd();

                return $this->redirect($this->generateUrl('stage_view', ['id' => $stage->getId()]));

            } else {
                $this->notificationService->warningStandard();
            }
        }

        return $this->render('stage/add-edit.html.twig', [
            'form' => $form->createView(),
            'stage' => $stage,
            'student' => $student,
        ]);
    }

    /**
     * @Route("/student/view/{id}", name="student_view_stage", methods={"GET"})
     *
     * @param Student $student
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function viewStudentAction(Student $student)
    {
        return $this->render('stage/view_student.html.twig', [
            'student' => $student,
        ]);
    }

    /**
     * @Route("/edit/{id}/", name="edit_stage", methods={"GET","POST"})
     *
     * @param Request $request
     * @param Stage $stage
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function editStageAction(Request $request, Stage $stage)
    {
        $form = $this->createForm(StageType::class, $stage);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            if ($this->stageManager->save($stage)) {
                $this->notificationService->successEdit();
            } else {
                $this->notificationService->warningStandard();
            }

            return $this->redirect($request->headers->get('referer'));
        }

        return $this->render('stage/add-edit.html.twig', [
            'form' => $form->createView(),
            'editMode' => true,
            'stage' => $stage,
            'student' => $stage->getStudent(),
        ]);
    }

    /**
     * @Route("/remove/{id}/", name="remove_stage", methods={"GET","POST"})
     *
     * @param Stage $stage
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function removeStageAction(Stage $stage)
    {
        if ($this->stageManager->remove($stage)) {
            $this->notificationService->successRemove();
        } else {
            $this->notificationService->warningStandard();
        }
        return $this->redirect($this->generateUrl('home_stage'));
    }
}
