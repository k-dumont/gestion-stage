<?php

namespace AppBundle\Event;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\AuthenticationEvents;
use Symfony\Component\Security\Core\Event\AuthenticationFailureEvent;
use AppBundle\Services\AntiBruteForce as AntiBruteForceService;

/**
 * Class AntiBruteForce
 * @package AppBundle\Event
 */
class AntiBruteForceListener implements EventSubscriberInterface
{

    const LOGIN_ROUTE_TO_CHECK = 'fos_user_security_check';

    /**
     * @var AntiBruteForceService
     */
    private $antiBruteForce;

    /**
     * @var RouterInterface
     */
    private $router;

    /**
     * @param AntiBruteForceService $antiBruteForce
     */
    public function __construct(AntiBruteForceService $antiBruteForce, RouterInterface $router)
    {
        $this->antiBruteForce = $antiBruteForce;
        $this->router = $router;
    }

    /**
     * @return array
     */
    public static function getSubscribedEvents()
    {
        return [
            AuthenticationEvents::AUTHENTICATION_FAILURE => 'onAuthenticationFailure',
            KernelEvents::REQUEST => ['beforeFirewall', 9],
        ];
    }

    /**
     * @param GetResponseEvent $event
     * @throws HttpException
     */
    public function beforeFirewall(GetResponseEvent $event)
    {
        $request = $event->getRequest();

        if ($request->isMethod(Request::METHOD_POST)) {

            $route = $this->router->match($request->getPathInfo());

            if (isset($route['_route']) && self::LOGIN_ROUTE_TO_CHECK === $route['_route']) {

                if ($this->antiBruteForce->isBruteForceRequest()) {

                    throw new HttpException(
                        429,
                        'Trop de tentatives de connexion échouée, veuillez réessayer plus tard.'
                    );
                }
            }
        }
    }

    /**
     * Log each failed tentative
     */
    public function onAuthenticationFailure()
    {
        $this->antiBruteForce->log();
    }
}
