import Forms from '../common/forms';
import $ from "jquery";

import Notifications from '../common/notifications';

window.$ = $;

/**
 * Entry point
 */
class App {

    constructor() {
        this.forms = new Forms($('form'));
        this.notifications = new Notifications();
    }
}

export default App;