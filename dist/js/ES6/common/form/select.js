import PerfectScrollbar from 'perfect-scrollbar';
import $ from "jquery";

/**
 * Class Select
 */
class Select {

    constructor(select) {
        this.select = select;
        this.wrapper = this.select.parent();
        this.init();
    }

    /**
     * Init Select
     */
    init() {
        this.buildHtml();
        this.bindEvent();
    }

    /**
     * Build all html
     */
    buildHtml() {
        this.container = $('<div>').addClass('container').appendTo(this.wrapper);
        this.values = $('<div>').addClass('values').appendTo(this.wrapper);
        this.isMultiple = this.select.attr('multiple') ? true : false;
        let selectedOption = null;

        $.each(this.select.find('option'), (index, option) => {

            let value = $(option).attr('value');
            let label = $(option).text();
            let optionDiv = $('<div>')
                .addClass('option')
                .attr('data-value', value)
                .text(label)
                .appendTo(this.values);

            if (this.isMultiple) {

                optionDiv.prepend($('<div>').addClass('checkbox'));

                if ($(option).attr('selected')) {
                    this.addValue(optionDiv);
                }

                optionDiv.click(() => {
                    if (optionDiv.hasClass('active')) {
                        this.removeValue(optionDiv)
                    } else {
                        this.addValue(optionDiv);
                    }
                });
            } else {
                if ($(option).attr('selected')) {
                    selectedOption = optionDiv;
                }

                if (index === 0) {
                    selectedOption = optionDiv;
                }

                optionDiv.click(() => {
                    this.hideList();
                    this.setValue(optionDiv);
                });
            }
        });

        if (!this.isMultiple) {
            if (selectedOption) {
                this.setValue(selectedOption);
            }
        }
    }

    /**
     * Bind Events
     */
    bindEvent() {
        this.container.click(() => {
            this.showList();

            $(document).bind('mouseup.materialFormSelect', (e) => {
                if (!this.values.is(e.target) && this.values.has(e.target).length === 0) {
                    this.hideList();
                }
            });
        });
    }

    /**
     * Show select options
     */
    showList() {
        this.values.addClass('active');
        this.ps = new PerfectScrollbar(this.values.get(0));
    }

    /**
     * Hide select options
     */
    hideList() {
        if (typeof this.ps == 'object') {
            this.ps.destroy();
        }

        this.values.removeClass('active');
        $(document).unbind('mouseup.materialFormSelect');
    }

    /**
     * Change the select value
     *
     * @param optionDiv
     */
    setValue(optionDiv) {
        this.select.val(optionDiv.attr('data-value'));
        this.container.text(optionDiv.text());
        this.values.find('.option').removeClass('active');
        optionDiv.addClass('active');
    }

    /**
     * Change the select value
     *
     * @param optionDiv
     */
    removeValue(optionDiv) {
        let array = this.select.val();
        let index = array.indexOf(optionDiv.attr('data-value'));

        if (index >= 0) {
            array.splice(index, 1);
        }

        this.select.val(array);

        optionDiv.removeClass('active');
        this.container.text(this.getLabels().join(', '));
    }

    /**
     * Change the select value
     *
     * @param optionDiv
     */
    addValue(optionDiv) {
        let array = this.select.val();
        array.push(optionDiv.attr('data-value'));

        this.select.val(array);

        optionDiv.addClass('active');
        this.container.text(this.getLabels().join(', '));
    }

    /**
     * Get labels
     *
     * @returns {Array}
     */
    getLabels() {
        let returnArray = [];
        let options = this.values.find('.option.active');

        options.each(function() {
            returnArray.push($(this).text());
        });

        return returnArray;
    }
}

export default Select;