import Select from "../common/form/select";
import $ from "jquery";

/**
 * Class Form
 */
class Forms {

    constructor(element) {
        this.element = element;
        this.initInputs();
        this.initSelects();
    }

    initInputs() {

        let inputs = this.element.find('.input-wrapper input');

        inputs.each(function () {
            if ($(this).val()) {
                $(this).parent().addClass("active");
            }
        });

        inputs
            .focusin(function () {
                $(this).parent().addClass("active");
            })
            .focusout(function () {
                if (!$(this).val()) {
                    $(this).parent().removeClass("active");
                }
            });
    }

    initSelects() {

        let selects = this.element.find('.select-wrapper select');

        selects.each(function () {
            new Select($(this));
        });
    }

}

export default Forms;