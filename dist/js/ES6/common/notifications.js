
/**
 * Class Notifications
 */
class Notifications {

    constructor() {
        this.bindEvents();
    }

    bindEvents() {
        $('.messages .alert .icon-cross').click(function () {
            $(this).parent().addClass('closed');
        })
    }
}

export default Notifications;